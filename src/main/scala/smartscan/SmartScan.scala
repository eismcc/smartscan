package smartscan

import collection.mutable.{ListBuffer, Stack}
import java.util.concurrent.atomic.AtomicInteger
import io.BufferedSource
import collection.mutable
import collection.immutable.HashMap

object SmartScan {

  def process(source: BufferedSource) : java.util.HashMap[String, Symbol] = {
    val ngrams = new java.util.HashMap[String, Symbol]

    // TODO: currently assumes \n delimited documents
    for( ln <- source.getLines ) {
      if (ln.equals("end")) {
        return ngrams
      }
      val text = ln.trim
      val delim = ','// findDelim(text)(0)._1
      val parts = text.split(delim).filterNot(x => x.isEmpty || x.equals(""))
      if (parts.size > 2) {
        var w1 = parts(0)
        var w2 = parts(1)
        val stack = new Stack().pushAll(parts.slice(2,parts.size).reverse)
        while (!stack.isEmpty) {
          val word = stack.pop
          val key = getKeyFromGrams(w1,w2)
          if (!ngrams.containsKey(key)) {
            ngrams.put(key, new Symbol(word))
          } else {
            ngrams.get(key).apply(w1, w2, word)
          }
          w1 = w2
          w2 = ngrams.get(key).toString()
        }
      }
    }

    ngrams
  }

  /** *
    * Find delimiter automatically in text.
    *
    * Algorithm:
    *   1) catalog each character and its positions in the text
    *   2) sort by frequency of spacing (more regular, less random is higher score)
    *       and assign a confidence.
    *
    * @param text
    * @return
    */
  def findDelim(text: String) : List[(Char, Double)] = {
    scoreCharacterFrequencies(catalogCharacterPositions(text))
  }

  def scoreCharacterFrequencies(posMap: Map[Char, List[Int]]) : List[(Char, Double)] = {
    val charScores = List() ++ posMap.keySet.flatMap{ key =>
      List((key, getFrequencyScore(posMap.get(key).get)))
    }
    charScores.sortBy{case (key : Char, score: Double) => score}
  }

  def getFrequencyScore(positions: List[Int]) : Double = {
    0
  }

  def catalogCharacterPositions(text: String) : Map[Char, List[Int]] = {
    val posMap = new mutable.HashMap[Char, ListBuffer[Int]]()

    (0 until text.length).foreach{ idx =>
      val ch = text.charAt(idx)
      if (!posMap.contains(ch)) {
        posMap.put(ch, new ListBuffer[Int]())
      }
      posMap.get(ch).get.append(idx)
    }

    Map() ++ posMap.keySet.flatMap{ key =>
      Map(key -> posMap.get(key).get.toList)
    }
  }

  def getKeyFromGrams(w1: String, w2: String) : String = {
    "%s%s".format(w1, w2)
  }

  def main(args: Array[String]) {

    val ngrams = process(io.Source.stdin);

    import scala.collection.JavaConversions._
    val vars = ngrams.values().filter(_.isVar()).toList.sortBy(_.toString())
    if (vars.size > 0) {
      println("I found the following variables:")
      val startGroup = new SymbolGroup
      var group = startGroup
      vars.foreach{ symbol =>
        group = SymbolGroup.chain(group, symbol)
//        println(symbol.toSymNGram)
      }

      group = startGroup
      while (group != null) {
        println("Group:")
        group.symbols.foreach{ symbol =>
          println(symbol.toSymNGram())
        }
        println
        group = group.nextGroup
      }
    } else {
      println("I didn't find any variables.")
    }
  }
}

object SymbolGroup {
  def chain(group: SymbolGroup, symbol: Symbol) : SymbolGroup = {
    group.apply(symbol)
  }
}

class SymbolGroup {
  val symbols = new ListBuffer[Symbol]
  var nextGroup : SymbolGroup = null

  def apply(symbol: Symbol) : SymbolGroup = {
    if (isNextSymbol(symbol)) {
      symbols.append(symbol)
      this
    } else {
      nextGroup = new SymbolGroup().apply(symbol)
      nextGroup
    }
  }

  def isNextSymbol(symbol: Symbol) : Boolean = {
    symbols.size == 0 ||
    (symbols.last.getNextGrams()._1.eq(symbol.getParentGrams()._1)
      && symbols.last.getNextGrams()._2.eq(symbol.getParentGrams()._2))
  }
}

object Symbol {
  private val ctr = new AtomicInteger
  def getNextSymbol() : String = {
    return "SYM%d".format(ctr.incrementAndGet())
  }
}

class Symbol(text: String) {

  private var w1: String = null
  private var w2: String = null
  private var sym : String = null
  private var values : ListBuffer[String] = null

  def isVar() : Boolean = sym != null
  def getParentGrams() : (String, String) = (w1,w2)
  def getNextGrams() : (String,String) = (w2, toString())
  override def toString() : String = if (isVar) sym else text

  def apply(w1: String, w2: String, nextVal: String) {
    if (this.w1 != null && w1 != this.w1) {
      throw new IllegalArgumentException("different w1! %s %s".format(this.w1, w1))
    }
    if (this.w2 != null && w2 != this.w2) {
      throw new IllegalArgumentException("different w2! %s %s".format(this.w2, w2))
    }
    if (sym == null) {
      if (!nextVal.equals(text)) {
        sym = Symbol.getNextSymbol()
        values = new ListBuffer[String]
        values.append(text)
        values.append(nextVal)
        this.w1 = w1
        this.w2 = w2
        println("%s %s %s".format(w1, w2, nextVal))
      }
    } else {
      println("%s %s %s".format(w1, w2, nextVal))
      values.append(nextVal)
    }
  }

  def toSymNGram() : String = {
    if (isVar()) {
      "%s %s %s ==> [%s]".format(w1, w2, toString(), values.mkString(","))
    } else {
      "%s %s %s".format(w1, w2, toString())
    }
  }
}

