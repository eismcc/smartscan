#union is a crlf delimited collection of sentences
cat union | tr "[()[]{}\^\~:;\!\"\?],\t" " " | sed 's/  */ /g' > union2
cat union2 | java -jar target/smartscan-0.1.0-jar-with-dependencies.jar > occurs
cat occurs | cut -f1-2 -d ' ' | sort | uniq -c | sort -rnk 1 > freq
